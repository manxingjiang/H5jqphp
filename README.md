# H5+jq+php图片滑动验证

#### 介绍
H5+jq+php图片滑动验证

#### 软件架构
软件架构说明


#### 安装教程

1.  PHP


#### 使用说明

1.  前端使用touchmove触发改变移动模块位置
movep.on("touchmove", function(event) {
			if (!isMouseDown) return;
			// 获取触摸点的坐标
			var touch = event.originalEvent.touches[0];
			var x = touch.pageX-50;
			
			// 移动元素
			$("#slider").css("left", x);
			movep.css("left", x);
		});
2.  后端PHP GD库动态生成滑块
function img()
{
	$width = 300;//设置背景图片宽度
	$height = 150;//设置背景图片高度
	$sliderImg = rand(1,5);//随机更换背景图
	$im = imagecreatefromjpeg('./img/'.$sliderImg.'.jpg');
	//获取当前待修改图片像素（内置函数）
	$x = imagesx($im);
	$y = imagesy($im);

	//新建一个真彩色图像(内置函数)
	$image = imagecreatetruecolor($width, $height);
	
	//重采样拷贝部分图像并调整大小(内置函数)
	imagecopyresampled($image, $im, 0, 0, 0, 0, floor($width), floor($height), $x, $y);
	

	// 绘制滑块 40*140
	$sliderColor = imagecolorallocate($image, 190, 190, 190);
	$sliderX = rand(50, $width - 50);
	imagerectangle($image, $sliderX, 70, $sliderX + 40, 110, $sliderColor);



	// 存储滑块的正确位置（用于后续验证）
	$_SESSION['sliderPos'] = $sliderX;
	$_SESSION['sliderImg'] = $sliderImg;
	// 输出图片
	header("Content-Type: image/jpeg");
	//imagepng($image);
	imagejpeg($image);
	imagedestroy($image);
}

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
