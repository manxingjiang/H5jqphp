<?php
/**
 *  H5 + jq + php 图片滑动验证
 *
 *  @author mxj <745139633@qq.com>
 *
 */
 
// 启动会话 开启session很重要，如果不想用session，可以memcache或者Redis
session_start();

//获取背景图片
function img()
{
	$width = 300;//设置背景图片宽度
	$height = 150;//设置背景图片高度
	$sliderImg = rand(1,5);//随机更换背景图
	$im = imagecreatefromjpeg('./img/'.$sliderImg.'.jpg');
	//获取当前待修改图片像素（内置函数）
	$x = imagesx($im);
	$y = imagesy($im);

	//新建一个真彩色图像(内置函数)
	$image = imagecreatetruecolor($width, $height);
	
	//重采样拷贝部分图像并调整大小(内置函数)
	imagecopyresampled($image, $im, 0, 0, 0, 0, floor($width), floor($height), $x, $y);
	

	// 绘制滑块 40*140
	$sliderColor = imagecolorallocate($image, 190, 190, 190);
	$sliderX = rand(50, $width - 50);
	imagerectangle($image, $sliderX, 70, $sliderX + 40, 110, $sliderColor);



	// 存储滑块的正确位置（用于后续验证）
	$_SESSION['sliderPos'] = $sliderX;
	$_SESSION['sliderImg'] = $sliderImg;
	// 输出图片
	header("Content-Type: image/jpeg");
	//imagepng($image);
	imagejpeg($image);
	imagedestroy($image);
}
//获取移动图片
function getcimg(){
	
	$width = 300;//设置背景图片宽度
	$height = 150;//设置背景图片高度
	$sliderImg = $_SESSION['sliderImg']?$_SESSION['sliderImg']:1 ;
	$im = imagecreatefromjpeg('./img/'.$sliderImg.'.jpg');
	//获取当前待修改图片像素（内置函数）
	$x = imagesx($im);
	$y = imagesy($im);

	//新建一个真彩色图像(内置函数)
	$sourceImage = imagecreatetruecolor($width, $height);
	
	//重采样拷贝部分图像并调整大小(内置函数)
	imagecopyresampled($sourceImage, $im, 0, 0, 0, 0, floor($width), floor($height), $x, $y);
	 
	// 设置剪切区域，这里以中心42x42像素为例
	$x = $_SESSION['sliderPos'];
	$y = 70;//这个70要和前端设置一样的值，否则，位置会偏移，无法重合
	$croppedWidth = 42;
	$croppedHeight = 42;
	 
	// 剪切图片
	$croppedImage = imagecrop($sourceImage, ['x' => $x, 'y' => $y, 'width' => $croppedWidth, 'height' => $croppedHeight]);
	 
	// 输出图片
	header("Content-Type: image/png");
	imagepng($croppedImage);
	 
	// 释放内存
	imagedestroy($sourceImage);
	imagedestroy($croppedImage);
}
function verify(){
	// 验证用户操作的PHP脚本
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {//$_REQUST
		$sliderPos = $_REQUEST['sliderPos'] ?? 0;
		$correctPos = $_SESSION['sliderPos'] ?? 0;
		// 简单的验证逻辑：判断滑块位置是否在正确位置的一定范围内
		$isClose = abs($sliderPos - $correctPos) < 5;

		$result = [
			'success' => $isClose,
			'sliderPos'=>$sliderPos,
			'correctPos'=>$correctPos
		];
		header('Content-type:text/json;charset=UTF-8;');
		echo json_encode($result);}
}
//登录
function login()
{
	header('Content-type:text/json;charset=UTF-8;');
	$sliderPos = $_REQUEST['sliderPos'] ?? 0;
	$correctPos = $_SESSION['sliderPos'] ?? 0;
	// 简单的验证逻辑：判断滑块位置是否在正确位置的一定范围内
	$isClose = abs($sliderPos - $correctPos) < 5;
	if (!$isClose) {
		$retutrue = [
			"retcode"=> "606", 
			"msg"=> "验证码不通过"
		];
		echo json_encode($retutrue);die;
	}
	if(empty($_POST) || empty($_POST['username']) || empty($_POST['password'])){
		$retutrue = [
			"retcode"=> "9", 
			"msg"=> "账号或密码不能为空"
		];	
	} else{
		if($_POST['username'] != 'admin'){
			$retutrue = [
				"retcode"=> "1005",
				"msg"=> "密码错误或用户不存在"
			];
		}else if ($_POST['password'] != '123456')
		{
			$retutrue = [
				"retcode"=> "1005", 
				"msg"=> "密码错误或用户不存在"
			];
		}else{
			//登陆 成功
			$retutrue = [
				"retcode"=> "0",
				"msg"=> "登陆成功"
			];
		}
	}
	
	echo json_encode($retutrue);
}
if ($_GET['act']=='img') {
	img();
} else if ($_GET['act']=='getcimg'){
	getcimg();
}else if ($_GET['act']=='verify'){
	verify();
}else if ($_GET['act']=='login'){
	login();
}